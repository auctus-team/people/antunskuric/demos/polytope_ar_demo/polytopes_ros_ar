# ROS workspace for communicating polytopes with HoloLens 2


This repository contains the ROS workspace for communicating polytopes with HoloLens 2. It contains the following packages:

- **panda_capacity: the capacity solver for Panda robot** - [gitlab repo](https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/panda_capacity)
- franka_description: Panda robot definitions from Franka Emika - [github repo](https://github.com/frankaemika/franka_ros)
- ROS-TCP-Endpoint: TCP endpoint for communicating with HoloLens 2 - [github repo](https://github.com/Unity-Technologies/ROS-TCP-Endpoint)

<img src="src/panda_capacity/images/unity_visualisation.jpg" width="800px">

In practice this looks something like that:

<img src="src/panda_capacity/images/ar_poly_good.jpg">



# Setup instructions

Here are the steps to setup the demo. 
## Installation instructions

<details><summary>Read more</summary>

### Download the workspace
To run panda robot capacity calculation nodes first clone the repository and submodules:
```shell
git clone https://gitlab.inria.fr/auctus-team/people/antunskuric/demos/polytopes_ros_ar.git
cd polytopes_ros_ar
```

### ROS Dependencies
For visualizing the polytopes in RVIZ you will need to install the [jsk-rviz-plugin](https://github.com/jsk-ros-pkg/jsk_visualization)

```sh
sudo apt install ros-$ROS_DISTRO-jsk-rviz-plugins 
```

The code additionally depends on the pinocchio library. 

For choosing the polytope on the fly you will need to install the [dynamic_reconfigure](http://wiki.ros.org/dynamic_reconfigure) package.

```sh
sudo apt-get install ros-$ROS_DISTRO-rqt ros-$ROS_DISTRO-rqt-common-plugins
```

### Other dependencies



We strongly suggest using anaconda for installing the dependencies. If you do not have anaconda installed you can install it from [here](https://www.anaconda.com/products/individual).


#### Installing dependencies using conda
<details><summary>Read more</summary>

If you don't have pinocchio installed we suggest you to use Anaconda. Create the environment will all dependencies:

```bash
conda env create -f env.yaml 
conda activate panda_ar  # activate the environment
```
Once the environment is activated you are ready to go.
 </details>

#### Installing dependencies using system libraries

<details><summary>Read more</summary>

Install pinochio library from the ROS repository
```bash
sudo apt install ros-$ROS_DISTRO-pinocchio
```
Once you have `pinnochio` installed simply install the the pip package `pycapacity` and `CGAL`
```
pip install pycapacity cgal
```
And you should be ready to go.
 </details>


### Building the workspace
And then just build the workspace
```shell
cd ..
catkin build
source devel/setup.bash
```
If you're using anaconda, don't forget to call <br>
```
conda activate panda_ar
```
before you call `catkin build`

 </details>

## HoloLens 2 setup

HoloLens 2 should have the PandaCapacityAR app installed. The installation instructions can be found in [PandaCapacityAR](https://gitlab.inria.fr/auctus-team/people/antunskuric/ar_projects/pandacapacityar) repository.

<details><summary>Launch the app</summary>

### Launching the app
1) Power up the HoloLens headset and launch the application `PandaCapacityAR`. 
2) Once the application is running come close to the QR code and scan it (you might have to come very close to it ~10-20cm)
3) Once the QR code is scanned you should see the robot in the application 
4) You should be able to see the robot in its default configuration (almost completely vertical)

Your HoloLens 2 is now ready to communicate with the ROS nodes.


### QR codes


- You can either use the QR code next to the robot to spawn the AR robot on top of the real robot or you can use the plastified QR codes to spawn the AR anywhere in the room. 

<img src="src/panda_capacity/images/qr_codes.jpg" height="250px">

<details><summary>Read more</summary>

- Here is the QR code if you wanna print it yourself.

<img src="src/panda_capacity/images/qrcode.png" height="250px">

</details>

</details>

## Network setup - force IP address to `172.16.0.160`

<details><summary>Read more</summary>

In order to communicate with HoloLens you'll have to have to be connected to the same network `crur` and have the IP address `172.16.0.160`.

To force your IP address to `172.16.0.160` do these steps:

1) Open wifi network manager and open `crur` settings
<img src="src/panda_capacity/images/step1.png" width="500px">

2) Open IPV4 tab and 
  - set the IPv4 method to manual
  - enter the IP address `172.16.0.160`
  - enter the netmask `255.255.255.0`
  - enter the gateway `172.16.0.1`
<img src="src/panda_capacity/images/step2.png" width="500px">

3) reconnect to `crur`
<img src="src/panda_capacity/images/step3.png" width="500px">

You should be ready to go.

> By forcing the IP address you might lose the internet access, but for this experiment you will not need it anyway.


</details>

# Running the demo

The demo can be run in two modes:
- [Robot simulation](#in-simulation) - you will be able to see the polytopes in RVIZ and in the HoloLens 2 application
- [On real robot](#on-real-robot) - you will be able to see the polytopes of the real-robot in the HoloLens 2 application 


## In simulation

<details><summary>Read more</summary>

Once when you have everything installed you will be able to run the interactive simulation and see the polytope being visualised in real-time in `RVIZ` and in the HoloLens 2 application.

<img src="src/panda_capacity/images/panda_cap_ar.gif" width="800px">

To see a simulation with a panda robot and its different polytopes run the command in the terminal.

```shell
source ~/polytopes_ros_ar/devel/setup.bash 
roslaunch panda_capacity one_panda.launch
```

> If using anaconda, don't forget to call `conda activate panda_ar` before you call `roslaunch panda_capacity one_panda.launch`


If your Hololens 2 is connected to the same network as your computer (and you have the good ip address as shown in the section [Network setup](#network-setup---force-ip-address-to-172160160)), and if you've spawned the AR robot using the QR code, you should be able to see the robot in the HoloLens 2 application. The steps to spawn the AR robot are described in the section [HoloLens 2 setup](#hololens-2-setup)

See the video below for the demo of the simulation from the HoloLens 2 perspective.


<a href="https://gitlab.inria.fr/auctus-team/people/antunskuric/demos/polytopes_ros_ar/-/blob/master/src/panda_capacity/images/hololens.mp4">
<img src="src/panda_capacity/images/holo_setup.png" width="500px">
</a>

</details>

## On real robot

Visualising the polytope capacities of the real robot is very similar to the simulation. 

There are two ways to run the demo on the real robot:
1) Using only one computer and running everything on the same computer
2) Using two computers and running the capacity solver on a different computer than the robot control software

### All-in-one computer

<details><summary>Read more</summary>

In order to run the demo on the real robot using only one computer you will have to install the `franka_ros` and `libfranka` libraries. Here are the docs for installing the [franka_ros](https://frankaemika.github.io/docs/installation_linux.html) and [libfranka](https://frankaemika.github.io/docs/installation_linux.html#building-from-source) libraries.

Then you can open the terminal and run the `franka_control` node and the `panda_capacity` node.

```shell
# 1) source your franka_ros workspace
source ~/franka_ros/devel/setup.bash
# 2) set the namespace to panda
export ROS_NAMESPACE=panda
# 3) run the franka_control node (with the correct ip address)
roslaunch franka_control franka_control.launch robot_ip:=172.16.0.x load_gripper:=false 
```
> Make sure to replace the ip address with the ip address of your robot.

> Also make sure to unlock the robot and enable its FCI mode.

This node will publish the `panda/joint_states` that will be used wit the `panda_capacity` node.

Then you can open the second terminal and run the `panda_capacity` node.

```shell
# 0) source anaconda environment if using it
conda activate panda_ar
# 1) source your workspace
source ~/polytopes_ros_ar/devel/setup.bash
# 2) run the panda_capacity node (without publishing the joint states)
roslaunch panda_capacity one_panda.launch publish_joint_states:=false
```

And that should be it. You should be able to see the robot and the polytopes in `RVIZ``.


If your Hololens 2 is connected to the same network as your computer (and you have the good ip address as shown in the section [Network setup](#network-setup---force-ip-address-to-172160160)), and if you've spawned the AR robot using the QR code, you should be able to see the robot in the HoloLens 2 application. The steps to spawn the AR robot are described in the section [HoloLens 2 setup](#hololens-2-setup)

#### Demo rollout


You will be able to put the robot in the gravity compensation mode (on the real robot) and then move it by hand in space and see the polytopes being visualised in real-time in the HoloLens 2 application.

</details>

### Multiple computers

<details><summary>Read more</summary>

If you're running your robot control software on one PC (like `leon`), you can visualise the polytopes using the `panda_capacity` node on a different PC.

There are just few steps to follow:

On the first computer (the one running the robot control software like `torque_qp`, `velocity_qp` or `lmpc`) make sure to set the `ROS_IP` environment variable.
```shell
export ROS_IP=172.16.0.x
```
where you should replace `x` with the last number of your IP address.

On the second computer (the one running the `panda_capacity` node) make sure to set the `ROS_IP` and `ROS_MASTER_URI` environment variables in each terminal you open.
```shell
export ROS_IP=172.16.0.y
export ROS_MASTER_URI=http://172.16.0.x:11311
```
where you should replace `x` with the last number of your IP address of the first computer and `y` with the last number of the IP address of the second computer.

Then you can open the terminal and run the `panda_capacity` node.

```shell
# 0) source anaconda environment if using it
conda activate panda_ar 
# 1) source your workspace
source ~/polytopes_ros_ar/devel/setup.bash
# 2) set the ros variables
export ROS_IP=172.16.0.y
export ROS_MASTER_URI=http://172.16.0.x:11311
# 3) run the panda_capacity node (without publishing the joint states and robot_description)
roslaunch panda_capacity one_panda.launch publish_joint_states:=false publish_robot_description:=false
```

#### Demo rollout

Use your robot control software on the first PC to move the robot and see the polytopes being visualised in real-time in the HoloLens 2 application.

</details>

## Choosing the polytope on the fly

<details><summary>Read more</summary>

To choose the polytope you want to visualise, open a new shell and run the command:
```shell
source devel/setup.bash 
rqt
```


<img src="src/panda_capacity/images/rqt_gui.png">

As shown by the image above, the gui allows you to:

1) to choose the polytope on the fly:
    - force polytope (`force`)
    - velocity polytope (`velocity`)
    - acceleration polytope (`acceleration`)
    - jerk polytope (`jerk`)
    - reachable space approximation within horizon 
        - convex polytope based approximation (`reachable_space_convex`)
        - nonconvex approximation (`reachable_space_nonconvex`)

<img src="src/panda_capacity/images/polytopes.png">

2) to choose the scaling factor for the polytope
    - either use the default values
        - force polytope: 1000
        - velocity polytope: 10
        - acceleration polytope: 500
        - jerk polytope: 20000
    - or set it manually using the slider
3) to choose the horizon length 
    - used only for the reachable space approximations


</details>
